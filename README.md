[Seleccion de Bandas](https://mamalemin-sdb.herokuapp.com/)
=========
### Aplicación para seleccionar bandas a través de votos en facebook
Cuyo destino es un iframe/pestaña dentro de una fanpage

* * *

Enlaces de interes:
- [Pro Git](http://www.librosweb.es/pro_git/) El libro oficial de git en español
- [Intro a la prog. en facebook V] (http://gnoma.es/blog/introduccion-a-la-programacion-en-facebook-v/) Apartado en el cual se explica como usar php-sdk
- [Agregar una pestaña a fanpage] (https://developers.facebook.com/docs/appsonfacebook/pagetabs/) https://www.facebook.com/dialog/pagetab?app_id=LA_APP_ID_DE_FACEBOOK&next=LA_URL_DE_HEROKU Esta misma url puede ser usada para solicitar a los usuarios a instalar la pestaña app. 
- [appfog] (https://www.appfog.com/) cloud server con buen soporte para php y mysql
- [Deploying a Simple PHP Application to AppFog from Codenvy](http://www.youtube.com/watch?v=fra82bqke7I)
- [Curso: creación de aplicaciones para facebook](http://loquenecesita.com/category/curso-en-video-aplicaciones-facebook/)
- [Curso de desarrollo de aplicaciones Facebook con SDK Javascript] (http://www.internoma.com/blog/curso-de-desarrollo-de-aplicaciones-facebook-con-sdk-javascript/)
- [heroku: desplegar aplicaciones](http://iloo.wordpress.com/2012/06/03/heroku-desplegar-aplicaciones/)
- [Integración de facebook con un sitio web](https://developers.facebook.com/docs/guides/web/) Documentación oficial en inglés.
- ¿Tenés un link muy bueno? Colocalo acá así queda para todos... 

